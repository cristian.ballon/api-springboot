package com.api.ballon.demo.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.api.ballon.demo.dtos.CreateDto;
import com.api.ballon.demo.dtos.CreateRequirementDto;
import com.api.ballon.demo.dtos.RequirementDto;
import com.api.ballon.demo.services.RequirementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.validation.Valid;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("requirements")
public class RequirementController {
    /*
     * ToDO
     * implementar swagger
     */

    @Autowired
    private RequirementService requirementService;

    @GetMapping()
    public ArrayList<RequirementDto> records() {
        return requirementService.getAllRequirements();
    }

    @GetMapping(path = "/{id}")
    public RequirementDto record(@PathVariable("id") Long id) {
        return requirementService.getRequirementById(id);
    }

    @PostMapping(consumes = {"multipart/form-data"})
    public CreateDto save( @RequestPart("file") MultipartFile file,  @RequestPart("requirement") String requirement) throws JsonMappingException, JsonProcessingException {
        if (file.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File must not be empty");
        }
        CreateRequirementDto createRequirementDto = new ObjectMapper().readValue(requirement, CreateRequirementDto.class);
        return requirementService.saveRequirement(createRequirementDto, file);
    }

    @PutMapping(path = "/{id}")
    public CreateDto update(@PathVariable Long id, @Valid @RequestBody CreateRequirementDto entity) {
        return requirementService.updateRequirement(id, entity);
    }

    @DeleteMapping(path = "/{id}")
    public Boolean destroy(@PathVariable("id") Long id) {
        return requirementService.destroyRequirement(id);
    }

}
