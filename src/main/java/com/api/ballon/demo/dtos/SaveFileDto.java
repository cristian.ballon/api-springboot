package com.api.ballon.demo.dtos;

public class SaveFileDto {
    private String message;
    private Boolean success;
    private String fileUrl;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public SaveFileDto(String message, Boolean success, String fileUrl) {
        this.message = message;
        this.success = success;
        this.fileUrl = fileUrl;
    }
}
