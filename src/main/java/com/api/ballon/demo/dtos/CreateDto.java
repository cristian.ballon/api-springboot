package com.api.ballon.demo.dtos;

public class CreateDto {
    
    private String message;
    private Boolean success;

    public CreateDto(String string, boolean b) {
        this.message = string;
        this.success = b;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
