package com.api.ballon.demo.dtos;

public class RequirementDto {

    private Long id;

    private String name;

    private String description;

    private String fileUrl;

    private Long userApplicantId;

    /** generate getters and setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Long getUserApplicantId() {
        return userApplicantId;
    }

    public void setUserApplicantId(Long userApplicantId) {
        this.userApplicantId = userApplicantId;
    }
}
