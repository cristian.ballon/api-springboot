package com.api.ballon.demo.services;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.api.ballon.demo.repositories.IRequirementRepository;
import com.api.ballon.demo.dtos.CreateDto;
import com.api.ballon.demo.dtos.CreateRequirementDto;
import com.api.ballon.demo.dtos.RequirementDto;
import com.api.ballon.demo.dtos.SaveFileDto;
import com.api.ballon.demo.entities.RequirementEntity;

import java.nio.file.Path;
import org.slf4j.Logger;

@Service
public class RequirementService {

    @Value("${file.upload-dir}")
    private String uploadDir;
    private static final Logger logger = LoggerFactory.getLogger(RequirementService.class);

    @Autowired
    private IRequirementRepository requirementRepository;
    /*
     * @Autowired
     * private IRequirementMyBatisRepository myBatisRepository;
     */

    @SuppressWarnings("null")
    public Boolean destroyRequirement(Long id) {
        try {
            requirementRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            logger.error("Error deleting requirement: " + e.getMessage(), e);
            return false;
        }
    }

    public ArrayList<RequirementDto> getAllRequirements() {

        ArrayList<RequirementDto> records = new ArrayList<>();

        var data = requirementRepository.findAll();
        data.forEach(item -> {
            RequirementDto record = new RequirementDto();
            record.setId(item.getId());
            record.setName(item.getName());
            record.setDescription(item.getDescription());
            record.setFileUrl(item.getFileUrl());
            record.setUserApplicantId(item.getUserApplicantId());
            records.add(record);
        });
        return records;
    }

    @SuppressWarnings("null")
    public RequirementDto getRequirementById(Long id) {
        var item = requirementRepository.findById(id).get();
        RequirementDto record = new RequirementDto();
        record.setId(item.getId());
        record.setName(item.getName());
        record.setDescription(item.getDescription());
        record.setFileUrl(item.getFileUrl());
        record.setUserApplicantId(item.getUserApplicantId());
        return record;
    }

    /*metodo para actualizar RequirementEntity */
    @SuppressWarnings("null")
    public CreateDto updateRequirement(Long id, CreateRequirementDto requirement) {
        try {
            var record = requirementRepository.findById(id).get();
            record.setName(requirement.getName());
            record.setDescription(requirement.getDescription());
            record.setUserApplicantId(requirement.getUserApplicantId());
            requirementRepository.save(record);
            return new CreateDto("Requirement updated successfully", true);
        } catch (Exception e) {
            logger.error("Error updating requirement: " + e.getMessage(), e);
            return new CreateDto("Error updating requirement", false);
        }
    }

    public CreateDto saveRequirement(CreateRequirementDto requirement, MultipartFile file) {
        try {

            var successFile = saveFile(file);

            if (!successFile.getSuccess())
                return new CreateDto("Error saving file", false);

            var record = new RequirementEntity();
            record.setName(requirement.getName());
            record.setDescription(requirement.getDescription());
            record.setFileUrl(successFile.getFileUrl());
            record.setUserApplicantId(requirement.getUserApplicantId());
            requirementRepository.save(record);

            return new CreateDto("Requirement created successfully", true);
        } catch (Exception e) {
            logger.error("Error creating requirement: " + e.getMessage(), e);
            return new CreateDto("Error creating requirement", false);
        }

    }

    private SaveFileDto saveFile(MultipartFile file) {
        try {
            // Guarda el archivo en la ruta especificada
            String filename = file.getOriginalFilename(); // Obtiene el nombre original del archivo

            // Genera un nuevo nombre de archivo si ya existe
            Path path = Paths.get(uploadDir + filename);
            String newFilename = filename;
            int counter = 0;
            while (Files.exists(path)) {
                counter++;
                String fileExtension = "";
                int dotIndex = newFilename.lastIndexOf('.');
                if (dotIndex > 0) {
                    fileExtension = newFilename.substring(dotIndex);
                    newFilename = newFilename.substring(0, dotIndex);
                }
                newFilename = newFilename + "(" + counter + ")" + fileExtension;
                path = Paths.get(uploadDir + newFilename);
            }

            Files.copy(file.getInputStream(), path); // Guarda el archivo en el disco
            return new SaveFileDto("File saved successfully", true, path.toString());
        } catch (Exception e) {
            logger.error("Error al guardar el archivo: " + e.getMessage(), e);
            return new SaveFileDto("Error saving file", false, "");
        }
    }
}
