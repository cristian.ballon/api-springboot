package com.api.ballon.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.ballon.demo.entities.RequirementEntity;

@Repository
public interface IRequirementRepository extends JpaRepository<RequirementEntity, Long> {
    
}
