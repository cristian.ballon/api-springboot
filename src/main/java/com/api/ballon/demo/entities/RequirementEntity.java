package com.api.ballon.demo.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "requirements")
public class RequirementEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false, name = "file_url")
    private String fileUrl;

    @Column(nullable = false, name = "user_applicant_id")
    private Long userApplicantId;

    /** generate getters and setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Long getUserApplicantId() {
        return userApplicantId;
    }

    public void setUserApplicantId(Long userApplicantId) {
        this.userApplicantId = userApplicantId;
    }

    /**ToDo
     * implementar libreria Lombok
     */
    
}
